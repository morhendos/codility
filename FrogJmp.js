function solution(X, Y, D) {
  if(X>0 && Y>0 && D>0 && X<=Y &&
    (Number(X) === X && X % 1 === 0) &&
    (Number(Y) === Y && Y % 1 === 0) &&
    (Number(D) === D && D % 1 === 0) ) {
      var result = (Y-X) / D;
      return Math.ceil(result);
  } else {
      return -1;
  }
}



console.log(solution(10,85,30));
console.log(solution(10,105,30));
console.log(solution(10.1,105.12,4));
