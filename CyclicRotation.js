function solution(K, A) {

  var ln = K.length;
  var tmp = [];

  if (ln < A) {
    A = A % ln;
  }

  tmp = K.slice(ln-A);
  K.splice(ln-A,A);

  return tmp.concat(K);
}

//tests
console.log(solution([1,2,3,4,5,6,7],1));
console.log(solution([2,2,3,4,5,6,7],1));
console.log(solution([3,2,3,4,5,6,7],1));
