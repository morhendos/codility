function solution(N) {
  if(N<=0) {
    return -1;
  }
  else {
    var binary = N.toString(2);
    var start = false;
    var longest =  0;
    var candidate =  0;

    for(var i=0; i<binary.length; i++) {

      if(start) {
        if(binary[i]==0) {
          candidate += 1;
        }
        else {
          start = false;
          if(longest < candidate) {
            longest = candidate;
          }
        }
      }

      if(binary[i]==1) {
        start = true;
        candidate =  0;
      }

    }

    return longest;
  }

}

//tests
console.log('result:',solution(529));
console.log('result:',solution(0));
