function solution(A) {
  if(A instanceof Array) {
    if(A.length >=0 && A.length<100001) {
      var B = [];
      var result = -1;
      B[0] = true;
      for(var i=0; i<A.length; i++) {
        B[A[i]] = true;
      }

      for(var i=1; i<A.length+1; i++) {
        if(B[i] != true) {
          result = i;
        }
      }

      if(result==-1) {
        result = A.length+1;
      }
      return result;
    }
    else {
      return -1;
    }
  }
  else {
    return -1;
  }
}

console.log(solution([2,3,5,1]));
console.log(solution([1,3]));
console.log(solution([1]));
console.log(solution([2]));
console.log(solution([]));
console.log(solution(1));
