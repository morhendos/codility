function solution(A) {
  var candidates= {};
  for(var i=0; i<A.length; i++) {
    if(!candidates[A[i]]) {
      candidates[A[i]] = 1;
    } else {
      delete candidates[A[i]];
    }
  }
  return Object.keys(candidates)[0];
}

//tests
console.log('result:',solution([9, 3, 9, 3, 9, 7, 9] ));
