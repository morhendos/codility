function solution(A) {
  var i, l, sum = [], sum_rev = [], result1, result2, best = -1;

  if(A instanceof Array) {

    l=A.length;

    if(l>1) {
      for(i = 0, l = A.length; i < l; i++) {
        if(i==0) {
          sum[i] = A[i];
          sum_rev[i] = A[l-1];
        }
        else if (i < l-1) {
          sum[i] = sum[i-1]+A[i];
          sum_rev[i] = A[l-1-i] + sum_rev[i-1];
        }
      }

      for(i = 0, l = sum.length/2; i < l; i++) {
        result1 = sum[i] - sum_rev[l*2-i-1];
        result2 = sum[l*2-i-1] - sum_rev[i];

        if(best > Math.abs(result1) || best===-1) {
          best = Math.abs(result1);
        }
        if(best > Math.abs(result2)) {
          best = Math.abs(result2);
        }

      }
    }
  }

  return best;
}

//tests
console.log(solution([3, 1, 2, 4, 3, 10],1));
console.log(solution([13, 21, 2, 4, 3, 10, 1, 20, 12]));
console.log(solution([1]));
console.log(solution([]));
console.log(solution());
console.log(solution('asd'));
